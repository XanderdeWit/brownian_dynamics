clear
run ../initialize.m

nbins=50;
gaussMax=4;
edges=linspace(-gaussMax,gaussMax,nbins);
steps=100;
Ns=round(logspace(1,6,steps));
errorsBM=zeros(steps,1);
errorsCL2=zeros(steps,1);
errorsCL6=zeros(steps,1);
errorsCL12=zeros(steps,1);

for i=1:steps
    N=Ns(i);

    gaussBM=getBoxMuller(N);
    gaussCL2=getCentralLimit(N,2);
    gaussCL6=getCentralLimit(N,6);
    gaussCL12=getCentralLimit(N,12);
    
    errorsBM(i)=getError(gaussBM,edges);
    errorsCL2(i)=getError(gaussCL2,edges);
    errorsCL6(i)=getError(gaussCL6,edges);
    errorsCL12(i)=getError(gaussCL12,edges);

    if ismember(i,[1,41,60,100])
        doPlot(gaussBM,gaussCL12,edges,gaussMax)
        title(['N=' num2str(N)])
    end
end

figure
loglog(Ns,errorsCL2,'LineWidth', 1.5,'DisplayName', 'Central limit $n=2$')
hold on
loglog(Ns,errorsCL6,'LineWidth', 1.5,'DisplayName', 'Central limit $n=6$')
loglog(Ns,errorsCL12,'LineWidth', 1.5,'DisplayName', 'Central limit $n=12$')
loglog(Ns,errorsBM,'LineWidth', 1.5,'DisplayName', 'Box-Muller')
grid minor
xlabel('$N$')
ylabel('RMS error')
legend('Location','southwest')

function doPlot(gaussBM,gaussCL,edges,gaussMax)
    figure
    histogram(gaussBM,edges,'Normalization','pdf','EdgeAlpha', 0.6, 'FaceAlpha', 0.2, 'DisplayName', 'Box-Muller')
    hold on
    histogram(gaussCL+1,edges+1,'Normalization','pdf','EdgeAlpha', 0.6, 'FaceAlpha', 0.2, 'DisplayName', 'Central limit $n=12$')
    grid minor
    legend
    [x0,y0]=getGaussTheory(1000,-gaussMax,gaussMax,1,0);
    plot(x0,y0,'Color', '#0072BD', 'LineWidth', 1.5)
    [x1,y1]=getGaussTheory(1000,-gaussMax+1,gaussMax+1,1,1);
    plot(x1,y1,'Color', '#D95319', 'LineWidth', 1.5)
end

function error=getError(gauss,edges)
    [nhist,ehist]=histcounts(gauss,edges,'Normalization','pdf');
    xhist=0.5*(ehist(1:end-1)+ehist(2:end));
    nhistT=gaussTheory(xhist,1,0);
    error=sqrt(mean((nhist-nhistT).^2));
end

function gauss=getBoxMuller(N)
    xi=rand(N,1);
    xim=zeros(N,1);
    for i=1:2:N-1
        xim(i)=sqrt(-2*log(xi(i)))*cos(2*pi*xi(i+1)); 
        xim(i+1)=sqrt(-2*log(xi(i)))*sin(2*pi*xi(i+1));
    end
    gauss=xim;
end

function gauss=getCentralLimit(N,n)
    x_gauss=zeros(N,1);
    for i=1:N
        rands_uniform = rand(n, 1);
        x_gauss(i) = sum(rands_uniform) - 0.5*n;
    end
    gauss=x_gauss;
end

function [x,y]=getGaussTheory(fineness,start,stop,sig,ave)
    x=linspace(start,stop,fineness);
    y=gaussTheory(x,sig,ave);
end

function y=gaussTheory(x,sig,ave)
    y=(1/sqrt(2*pi*sig^2)).*exp(-(x-ave).^2./(2*sig^2));
end