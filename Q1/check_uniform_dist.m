clear
run ../initialize.m

steps=100;
nbins=10;
Ns=round(logspace(2,7,steps));
stds=zeros(steps,1);
errors=zeros(steps,1);
for i=1:steps
    N=Ns(i);
    x=rand(N,1);
    stds(i)=std(x);
    errors(i)=getError(x,nbins);
    
    if ismember(i,[1,30,60,100])
        figure
        histogram(x,nbins,'Normalization','pdf')
        title(['Histogram uniform distribution for N=' num2str(N)])
    end
end
std_theory=sqrt((1/3)*1^3-0.5^2);
disp(['Theoretical standard deviation = ' num2str(std_theory)])

figure
semilogx(Ns,stds,'LineWidth', 1)
hold on
line([Ns(1),Ns(end)],[std_theory,std_theory],'Color','red','LineStyle','--','LineWidth', 1)
title('Standard deviation of uniform distribution')
xlabel('$N$')
ylabel('$\sigma$')

figure
loglog(Ns,errors,'LineWidth', 1)
hold on
xlabel('$N$')
ylabel('RMS error')

function error=getError(x,nbins)
    [nhist,~]=histcounts(x,nbins,'Normalization','pdf');
    nhistT=1;
    error=sqrt(mean((nhist-nhistT).^2));
end