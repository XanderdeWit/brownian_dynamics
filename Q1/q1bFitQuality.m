% Written by Joris Dalderup on 2019-09-29
% License: MIT

% This function determines the quality of a fit based on the R-squared
% value(aka coefficient of determination). In this case, higher is better.
% See en.wikipedia.org/wiki/Coefficient_of_determination
function r2 = q1bFitQuality(data, fitted)

if length(data) ~= length(fitted)
    error("Data and fit must be of equal size");
end

SStot = sum((data - mean(data)).^2);
SSres = sum((data - fitted).^2);

r2 = 1 - SSres/SStot;

end