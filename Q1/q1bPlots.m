clear
% Written by Joris Dalderup on 2019-09-29
% License: MIT

% Create the random numbers for Question 1b, fit them to a Gaussian and
% determine the fit accuracy using q1bFitQuality.m

% Generate random data
N = 1e4; % Amount of random data points per set
randomSet1 = normrnd(0, 1, N, 1);
randomSet2 = normrnd(1, 1, N, 1);

h1 = histogram(randomSet1, 'EdgeAlpha', 0.6, 'FaceAlpha', 0.2, ...
    'DisplayName', 'Dataset 1');
hold on
h2 = histogram(randomSet2, 'EdgeAlpha', 0.6, 'FaceAlpha', 0.2, ...
    'DisplayName', 'Dataset 2');

h1Bins = conv(h1.BinEdges, [1, 1]/2, 'valid');
h2Bins = conv(h2.BinEdges, [1, 1]/2, 'valid');

fit1 = fit(h1Bins', h1.Values', 'gauss1');
fit2 = fit(h2Bins', h2.Values', 'gauss1');

fit1Values = feval(fit1, h1Bins');
r21 = q1bFitQuality(h1.Values', fit1Values);
plot(h1Bins', fit1Values, 'Color', '#0072BD', 'LineWidth', 1.5, ...
    'DisplayName', sprintf('Gaussian fit 1\n(R^2 = %.3f)', r21));
fit2Values = feval(fit2, h2Bins');
r22 = q1bFitQuality(h2.Values', fit2Values);
plot(h2Bins', fit2Values, 'Color', '#D95319', 'LineWidth', 1.5, ...
    'DisplayName', sprintf('Gaussian fit 2\n(R^2 = %.3f)', r22));
grid minor
legend
saveas(gcf, 'q1b.eps')

hold off;