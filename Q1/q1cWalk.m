clear
% Written by Joris Dalderup on 2019-09-29
% License: MIT

% Do a random walk and check the paramters
sum(arrayfun(@(a) doRandomWalk(100), 1:10000).^2)/10000

function dist = getWalk()
    walk = randsample(1:4, 100, true);
    x = sum(walk==1) - sum(walk==2);
    y = sum(walk==3) - sum(walk==4);
    dist = sqrt(x^2 + y^2);
end