clear
run ../initialize.m
steps=100;
N=100;
Ls=round(logspace(1,6,steps));
arsqr=zeros(steps,1);
for i=1:steps
    L=Ls(i);
    rsqr=zeros(L,1);
    for j=1:L
        rsqr(j)=doRandomWalk(N)^2;
    end
    arsqr(i)=mean(rsqr);
end

rsqr_theory=sqrt(N);
disp(['Theoretical standard deviation = ' num2str(rsqr_theory)])

figure
semilogx(Ls,sqrt(arsqr))
ylim([8 12])
xlim([Ls(1) Ls(steps)])
hold on
line([Ls(1),Ls(end)],[rsqr_theory,rsqr_theory],'Color','red','LineStyle','--')
xlabel('$L$')
ylabel('$\sqrt{<R^2>}$')
saveas(gcf, 'Random_Walk_Standard_Deviation.eps', 'epsc')
error=zeros(steps,1);
for i=1:steps
    error(i)=abs(sqrt(arsqr(i))-sqrt(N));
end
figure
semilogx(Ls,error)
xlim([Ls(1) Ls(steps)])
saveas(gcf, 'Random_Walk_error.eps', 'epsc')
function rsqr=doRandomWalk(N)
l=1;
xi=rand(N,1);
rw=[0 0];
for i=1:N
    if xi(i)<=0.25
        rw(1)=rw(1)+l;
    elseif (xi(i)>0.25) && (xi(i)<=0.50)
            rw(1)=rw(1)-l;
    elseif (xi(i)>0.5) && (xi(i)<=0.75)
             rw(2)=rw(2)+l;
    elseif xi(i)>0.75
             rw(2)=rw(2)-l;                
    end
end
rsqr=sqrt(rw(1)^2+rw(2)^2);
end