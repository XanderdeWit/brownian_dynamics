run ../initialize
% This script was written by Joris Dalderup on 2019-10-12


f = r{4};
dt = 10^-3;

b = sqrt(sum(diff(f, 1, 3).^2, 2));

LL = 1e5

figure(1)
p = plot((0:(length(f(1:LL)) - 1))*dt, b(1:LL, 1, 1), 'DisplayName', '$n = 1$')
p.Color(4) = .2;
hold on
plot((0:(length(f(1:LL)) - 1))*dt, cummean(b(1:LL, 1, 1)), 'DisplayName', 'cumulative mean, $n = 1$', 'LineWidth', 2.5)
hold off
xlabel('$t$ [-]')
ylabel('$l$ [-]')
legend('Location', 'northeast')
grid on
saveas(figure(2), 'q2d1.pdf')

figure(2)
p = plot((0:(length(f(1:LL)) - 1))*dt, b(1:LL, 1, 8), 'DisplayName', '$n = 8$')
p.Color(4) = .2;
hold on
plot((0:(length(f(1:LL)) - 1))*dt, cummean(b(1:LL, 1, 8)), 'DisplayName', 'cumulative mean, $n = 8$', 'LineWidth', 2.5)
hold off
xlabel('$t$ [-]')
ylabel('$l$ [-]')
legend('Location', 'northeast')
grid on
saveas(figure(2), 'q2d2.pdf')

l = sqrt(sum(diff(f, 1, 3).^2, 2));
chainav = mean(l, 1);
figure(3)
scatter(1:length(chainav), squeeze(chainav), 'DisplayName', 'time average bond length')

hold on
plot(0:(length(chainav) + 1), ones(1, length(chainav) + 2)*0.926, 'DisplayName', 'theory', 'LineStyle', '--')
hold off
legend('Location', 'southeast')
xlabel('$n$ [-]')
ylabel('$\langle l_n \rangle$ [-]')
grid minor
grid on
set(gca, 'box', 'on')
ylim([0, 1])
xlim([0, 17])
saveas(figure(3), 'q2d3.pdf')