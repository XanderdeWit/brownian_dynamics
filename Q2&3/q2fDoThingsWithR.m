run ../initialize
close all
% This script was written by Joris Dalderup on 2019-10-12

evLengths = zeros(1, length(deltaT));

for i = 1:length(deltaT)
    ri = r{i};
    time = (1:size(ri, 1))*1;
    lengths = sqrt(sum(diff(ri, 1, 3).^2, 2));

    evLengths(i) = mean(mean(lengths));
    %plot(time, lengths(:, 1, 1))
end

figure(1)
semilogx(deltaT, evLengths, 'DisplayName', '$t_{max} = 10^3$')
hold on
semilogx(deltaT(1:(end - 3)), ones(1, length(deltaT) - 3)*0.921318, 'DisplayName', 'theory')
hold off
xlabel('$\Delta t$ [-]')
ylabel('$I_0$ [-]')
legend('Location', 'northwest')
grid on
ylim([0, 2])
saveas(figure(1), 'q2fI0.eps', 'epsc')