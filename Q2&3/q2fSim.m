function r = q2fSim(deltaT, timeMax)
% Written by Joris Dalderup on 2019-09-30
% License: MIT

% Perform the simulation for Question 2
Np = 17; % Number of particles

N = 10^3*10^4; % Number of timesteps

deltaT = 10^-3;

N = timeMax/deltaT; % Number of timesteps

r = zeros(N, 3, Np);
r(1, :, :) = [(1:Np)/2; mod(1:Np, 2) - 0.5; (1:Np)*0];

%scatter(r(1, 1, :), r(1, 2, :));

%l2s=zeros(1,N);

tic


for n = 2:N
    if (mod(n, 1000) == 0)
        fprintf("Now at %i\n", n)
        toc
    end
    
    r(n, :, :) = r(n - 1, :, :) + computeForces(r(n - 1, :, :))*deltaT*3 + ...
        randomForces(r(n - 1, :, :), deltaT);
    
    
    %scatter3(r(n, 1, :), r(n, 2, :), r(n, 3, :));
    %hold on
    %plot3(squeeze(r(n, 1, :)), squeeze(r(n, 2, :)), squeeze(r(n, 3, :)));
    
    %b = diff(r(1:n, :, :), 1, 3);
    %l2 = mean(b(:, 1, :).^2 + b(:, 2, :).^2 + b(:, 3, :).^2);
    %l = sqrt(l2);
    
    %l2s(n)=mean(l2);
    
    %b = diff(r(1:n, :, :), 1, 3);
    %l2 = mean(b(:, 1, :).^2 + b(:, 2, :).^2 + b(:, 3, :).^2);
    %l = sqrt(l2);
    
    
    %fprintf("The mean of I^2 is %.3f, the std dev is %.3f\n", mean(l2), std(l2));
    
    %hold off
    %axis equal
    %xlim([-1, 10])
    %ylim([-3, 3])
    %zlim([-1, 1])
    %pause(5/1000)
end

%plot(N*dt,l2s);

function forces = computeForces(r)
    Np = length(r);
    forces = zeros(size(r));
    
    for i = 2:(Np - 1)
        forces(1, :, i) = r(1, :, i - 1) - 2*r(1, :, i) + r(1, :, i + 1);
    end
    
    forces(1, :, 1) = -r(1, :, 1) + r(1, :, 2);
    forces(1, :, Np) = r(1, :, Np - 1) - r(1, :, Np);
end

function forces = randomForces(r, deltaT)
    forces = normrnd(0, sqrt(deltaT*2), size(r));
end

end