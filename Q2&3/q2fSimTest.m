clear
close all
run ../initialize
% This script was written by Joris Dalderup on 2019-10-12

deltaT = [0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2, ...
    0.5, 1];
r = cell(1, length(deltaT));

for i = 1:length(deltaT)
    r{i} = q2fSim(deltaT(i), 1e3);
end