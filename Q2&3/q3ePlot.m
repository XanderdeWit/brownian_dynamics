close all

run ../initialize.m

f = r{4};

b = sqrt(sum((f(:, :, 16) - f(:, :, 1)).^2, 2));

b2 = b.^2;


figure(1)
plot((1:length(b))*10^-3, cumsum(b)'./(1:length(b)), 'DisplayName', '$\langle L \rangle$ (cumulative)')
ylim([0, 10])
xlabel('t [-]')
ylabel('$\langle L \rangle$ [-]')
grid minor
legend
saveas(figure(1), 'q3e1.eps', 'epsc')

figure(2)
plot((1:length(b))*10^-3, sqrt(cummean(b2)), 'DisplayName', '$\sqrt{\langle L^2 \rangle}$ (cumulative)')
hold on
plot((1:length(b))*10^-3, ones(1, length(b))*4, 'LineStyle', '--', 'DisplayName', 'theory')
hold off
ylim([0, 10])
xlabel('t [-]')
ylabel('$\sqrt{\langle L^2 \rangle}$ [-]')
grid minor
legend
saveas(figure(2), 'q3e2.eps', 'epsc')

figure(3)
plot((1:length(b))*10^-3, sqrt(cummean(b2) - cummean(b).^2), 'DisplayName', '$\sigma_L$ (cumulative)')
ylim([0, 4])
xlabel('t [-]')
ylabel('$\sigma_L$ [-]')
grid minor
legend
saveas(figure(3), 'q3e3.eps', 'epsc')