Nacor=10^3*1;

figure
hold on

tps=zeros(2,Np-1);
ft=fittype('exp(-x/tp)');
for p=1:(Np-1)
    kp=(pi*p)/Np;
    tp=1/(4*3*(sin(kp/2))^2);
    h=plot((1:Nacor)*deltaT,exp(-((1:Nacor)*deltaT)/tp),'LineWidth',0.5);
    scatter((1:20:Nacor)*deltaT,acors(1:20:Nacor,p),20,h.Color,'x')
    
    tps(1,p)=tp;
    tps(2,p)=coeffvalues(fit((1:Nacor)'*deltaT',acors(1:Nacor,p),ft,'StartPoint',tp));
end

box on
grid on
ylim([0,1])
xlabel('$t/\tau_{BD}$')
ylabel('$<q_p(0)q_p(t)>/<q_p^2>$')

figure
hold on
scatter(1:(Np-1),tps(2,:),20,'x','LineWidth',1)
plot(1:(Np-1),tps(1,:),'--','LineWidth',0.3)
set(gca,'yscale','log')
legend('Calculated $\tau_p$','Theoretical $\tau_p$')
xlabel('$p$')
ylabel('$\tau_p/\tau_{BD}$')
box on
grid on

figure
hold on
b2p=b2(5*10^3*10^3:end,:);
q2p=q2(5*10^3*10^3:end,:);
scatter((1:size(b2p,1))*deltaT,sum(cumsum(b2p,1),2)'./(1:size(b2p,1)),3,'s','filled')
scatter((1:size(q2p,1))*deltaT,sum(cumsum(q2p,1),2)'./(1:size(q2p,1)),3,'s','filled')
line([0,size(q2p,1)*deltaT],[16,16],'Color','red','LineStyle','--','LineWidth', 1)
legend('$|\vec{u}|^2$','$|\vec{q}|^2$')
xlabel('$t/\tau_{BD}$')
ylabel('Running average $|\vec{q}|^2$, $|\vec{u}|^2$ / $x_{BD}^2$')
xlim([0,size(b2p,1)*deltaT])
box on
grid on
