rstat=r((2*10^3*10^3):(10*10^3*10^3),:,:);

b = diff(rstat, 1, 3);

q=qFromB(b,Np);

acors=zeros(size(q,1),Np-1);
%plot autocorss
figure
hold on
for p=1:(Np-1)
    acors(:,p)=acorVec(q(:,:,p),true,1*10^3,deltaT);
end
box on
grid on
ylim([0,1])

q2=zeros(size(q,1),size(q,3));
b2=zeros(size(q,1),size(q,3));
for j=1:(Np-1)
    q2(:,j)=q(:,1,j).^2+q(:,2,j).^2+q(:,3,j).^2;
    b2(:,j)=b(:,1,j).^2+b(:,2,j).^2+b(:,3,j).^2;
end
disp(['<q1^2>+...<qn^2> = ' num2str(sum(mean(q2,1)))])
disp(['<b1^2>+...<bn^2> = ' num2str(sum(mean(b2,1)))])

function q=qFromB(b,Np)
    a=sqrt(2/Np);
    q=zeros(size(b));
    for p=1:(Np-1)
        sump=zeros(size(q(:,:,p)));
        for j=1:(Np-1)
            termpj=sin(pi*j*p/Np)*b(:,:,j);
            sump=sump+termpj;
        end
        q(:,:,p)=a*sump;
    end
end

%x is formatted like x(t,:)
function acor=acorVec(x,graph,Nacor,deltaT)

    acor=zeros(2*size(x,1)-1,1);
    for i=1:size(x,2)
        acor=acor+xcorr(x(:,1)-mean(x(:,1)));
    end
    acor=acor(size(x,1):end)/acor(size(x,1));

    if graph
        scatter((1:Nacor)*deltaT,acor(1:Nacor),3,'s','filled')
    end

end