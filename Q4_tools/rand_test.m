close all

res = arrayfun(@(a) dor(), 1:10000);

plot(res)
mean(res)
std(res)

function a = dor()

phi = rand(1, 16)*pi*2;
theta = acos(rand(1, 16)*2 - 1);

a = sqrt(sum(sum([sin(theta).*cos(phi); sin(theta).*sin(phi); cos(theta)], 2).^2));

end