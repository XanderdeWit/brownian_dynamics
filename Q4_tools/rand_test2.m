close all

[m, s, msq] = arrayfun(@(n) geta(n), 1:30);

plot(1:30, m)
hold on
plot(1:30, sqrt(2/pi*1:30))
hold on
plot(1:30, s)
plot(1:30, msq)

function [meana, stda, mnsq] = geta(n)

res = arrayfun(@(a) dor(n), 1:10000);
meana = mean(res);
stda = std(res);
mnsq = mean(res.^2)

function a = dor(n)

phi = rand(1, n)*pi*2;
theta = acos(rand(1, n)*2 - 1);

a = sqrt(sum(sum([sin(theta).*cos(phi); sin(theta).*sin(phi); cos(theta)], 2).^2));

end

end