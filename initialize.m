%Set typeface
set(0,'defaultTextInterpreter','latex')
set(0,'defaultLegendInterpreter','latex')
set(0,'defaultAxesTickLabelInterpreter','latex')

set(0,'DefaultAxesFontSize', 16)

set(0,'DefaultLegendAutoUpdate','off')